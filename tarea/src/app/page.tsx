import Image from "next/image";
import styles from "./page.module.css";
import NavBar from "./componentes/NavBar";
import Boton from "./componentes/Boton";
import SaltoLinea from "./componentes/SaltoLinea";
export default function Home() {
  return (
    <main >
      <div>
        <NavBar></NavBar>
      </div>

    <div className="container">
        <div className="letras">
        <h1>I’m Arnau Ros, a graphic designer & content creator based in Barcelona. Available for freelance & collaborations. </h1>
       </div>

       <div className="pajarito">
        <img src="/imagenes/img1.svg" alt="" />
       </div>

       
    </div>
    <div className="botones">
        <Boton title="Log In"></Boton>
        <Boton title="Sign Up" border></Boton>
     </div>


     <div className=" soloLinea">
      <SaltoLinea title="Projects" ></SaltoLinea>
      
      

     </div>


      
       
    </main>
  )
}
