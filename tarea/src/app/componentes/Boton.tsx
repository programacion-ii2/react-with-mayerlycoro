export default function Boton (props:{title:string,border?:boolean}){
    return(
            props.border?
            <button className="btn btn-border">{props.title}</button>
            :
            <button className="btn primary">{props.title}</button>
    )
}