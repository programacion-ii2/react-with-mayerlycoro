# ***PRÁCTICA 11***  📚  👩🏻‍💻

**-DATOS PERSONALES**
| **NOMBRE  🌻** | **MATERIA  💻**    | **AUXILIAR  🐱**                |
| :-------- | :------- | :------------------------- |
| *Mayerly Coro Canaviri* | *Diseño y Programacion* | *Sergio Apaza*|

```http
```

## ***PREGUNTA 1*** 👩🏻‍💻 🌻
- [X]     🌟 Realizar el Header de su template elegido
+ COMPLETADA  🙈🎉


```http

```
 

## ***PREGUNTA 2*** 👩🏻‍💻 🌻

- [X]    📘Identificar que componentes se usará a lo largo del proyecto y subirlos en un archivo readme.md.😎 😈  🤘

+ Boton.tsx
+ NavBar.tsx
+ SaltoLinea.tsx , un componente que se repite mucho
+ ImagenesCuadros.tsx componente que contiene imagenes 

aclarando que mi persona escogio un templay muy sencillo teniendo muy pocos componentes pero para poder en practica lo aprendido agregare componentes adicionales asi como botones y demas. 
```http

```
 

## ***PREGUNTA 3*** 👩🏻‍💻 🌻

- [X]    📘Crear un archivo README.md en el directorio principal del repositorio con la siguiente información:

• Created by: <Nombres y apellidos> (nombres y apellidos del estudiante).

• Título del proyecto (Nombre o título de su template).

• Link del Template en Figma.

• Link del repositorio principal (el que se creó en principio y donde se subían sus prácticas).

• Avances del template. Si lo prefieren😎 😈  🤘


1. Nombre del templay : Mayerly Coro

2. ⚠️***Aqui el Link del proyecto en Figma utilizando el template., presione la Lupita***     [  🔎 ](https://www.figma.com/file/CHyYoHbFW8VBWGaO0UO8OI/Arnau-Ros-%E2%80%A2-Portfolio-Template?type=design&node-id=0-1&mode=design&t=2Fo8JiJnwJaZzaBt-0)

3. ⚠️***Aqui el Link del nuevo repositorio en GITLAP.presione la Lupita***     [  🔎 ](https://gitlab.com/programacion-ii2/react-with-mayerlycoro)

4. Avances del templay 

- [x] Header 
- [ ] Seccion 1 
- [ ] Seccion 2
- [ ] Footer 


 ****TAREA COMPLETADA**** 🎉
 ```http

```

notita: la 4 nose como hacer auxi...... disculpitas 😭😭😭


